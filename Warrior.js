export default class Warrior {
  weapon;
  powerUp;

  constructor(name, power, life) {
    this.name = name;
    this.power = power;
    this.life = life;
    this.powerUp = this.power * 2;
  }

  attack(opponent, powerLevel) {
    let damage = powerLevel === "powerUp" ? this.powerUp : this.power;
    let lifeAfterAttack = (opponent.life -= damage);
    return `Le guerrier ${
      this.name
    } a réalisé une attaque de ${damage} de dégâts sur ${opponent.name}. ${
      opponent.name
    } ${
      lifeAfterAttack > 0
        ? `n'a plus que ${lifeAfterAttack} points de vie.`
        : `a perdu tous ses points de vie.`
    } `;
  }

  isAlive() {
    return this.life > 0
      ? `${this.name} est encore en vie !`
      : `${this.name} a été vaincu...`;
  }
}
