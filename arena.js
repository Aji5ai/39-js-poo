import Warrior from "./Warrior.js";
import WarriorSword from "./WarriorSword.js";
import WarriorAxe from "./WarriorAxe.js";
import WarriorSpear from "./WarriorSpear.js";

//Partie 1
/* const ulfric = new Warrior("Ulfric", 43, 100);
const tullius = new Warrior("Tullius", 55, 100);

console.log(tullius.attack(ulfric));
console.log(ulfric.isAlive());

console.log(ulfric.attack(tullius));
console.log(tullius.isAlive());

console.log(tullius.attack(ulfric));
console.log(ulfric.isAlive()); */

// Partie 2

/* const sword1 = new WarriorSword("Sokka", 18, 100);
const axe1 = new WarriorAxe("Nomdir", 36, 100);
const spear1 = new WarriorSpear("Ou Hon", 41, 100);

console.log(sword1.attack(axe1)); // Pas de dégâts doublés.
console.log(axe1.isAlive());

console.log(axe1.attack(sword1)); // Dégats doublés !!
console.log(sword1.isAlive());

console.log(spear1.attack(axe1)); // Dégats doublés !!
console.log(axe1.isAlive()); */

// Partie 3

// It's a draw
/* while (sword1.life > 0 && spear1.life > 0) {
  console.log(sword1.attack(spear1));
  console.log(spear1.attack(sword1));
  console.log(sword1.isAlive());
  console.log(spear1.isAlive());

  if (sword1.life <= 0 && spear1.life <= 0) {
    console.log("It's a draw");
  } else if (sword1.life <= 0 && spear1.life > 0) {
    console.log(`${spear1.name} wins`);
  } else if (sword1.life > 0 && spear1.life <= 0) {
    console.log(`${sword1.name} wins`);
  }
} */

// Ou Hon wins
/* while (axe1.life > 0 && spear1.life > 0) {
  console.log(axe1.attack(spear1));
  console.log(spear1.attack(axe1));
  console.log(axe1.isAlive());
  console.log(spear1.isAlive());

  if (axe1.life <= 0 && spear1.life <= 0) {
    console.log("It's a draw");
  } else if (axe1.life <= 0 && spear1.life > 0) {
    console.log(`${spear1.name} wins`);
  } else if (axe1.life > 0 && spear1.life <= 0) {
    console.log(`${axe1.name} wins`);
  }
}

 */
