import Warrior from "./Warrior.js";
import Weapon from "./Weapon.js";

export default class WarriorSword extends Warrior {
  constructor(name, power, life) {
    super(name, power, life);
    this.weapon = new Weapon("sword");
  }

  attack(opponent) {
    if (opponent.weapon.name === "spear") {
      return super.attack(opponent, "powerUp");
    } else {
      return super.attack(opponent, "power");
    }
  }
}
